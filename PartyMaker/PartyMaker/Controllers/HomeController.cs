﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PartyMaker.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "PandaKomanda";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "PandaKomanda";

            return View();
        }
    }
}