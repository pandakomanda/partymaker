﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PartyMaker.Models;
using PartyMaker.Models.Entities;

namespace PartyMaker.Controllers
{
    public class RepresentativeASController : Controller
    {
        private PartyMakerDBContext db = new PartyMakerDBContext();

        // GET: RepresentativeAS
        public ActionResult Index()
        {
            return View(db.RepresentativeASs.ToList());
        }

        // GET: RepresentativeAS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RepresentativeAS representativeAS = db.RepresentativeASs.Find(id);
            if (representativeAS == null)
            {
                return HttpNotFound();
            }
            return View(representativeAS);
        }

        // GET: RepresentativeAS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RepresentativeAS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RepresentativeASId,RepresentativeASType,RepresentativeASName,RepresentativeASRating,RepresetnativeASPhoto")] RepresentativeAS representativeAS)
        {
            if (ModelState.IsValid)
            {
                db.RepresentativeASs.Add(representativeAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(representativeAS);
        }

        // GET: RepresentativeAS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RepresentativeAS representativeAS = db.RepresentativeASs.Find(id);
            if (representativeAS == null)
            {
                return HttpNotFound();
            }
            return View(representativeAS);
        }

        // POST: RepresentativeAS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RepresentativeASId,RepresentativeASType,RepresentativeASName,RepresentativeASRating,RepresetnativeASPhoto")] RepresentativeAS representativeAS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(representativeAS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(representativeAS);
        }

        // GET: RepresentativeAS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RepresentativeAS representativeAS = db.RepresentativeASs.Find(id);
            if (representativeAS == null)
            {
                return HttpNotFound();
            }
            return View(representativeAS);
        }

        // POST: RepresentativeAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RepresentativeAS representativeAS = db.RepresentativeASs.Find(id);
            db.RepresentativeASs.Remove(representativeAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
