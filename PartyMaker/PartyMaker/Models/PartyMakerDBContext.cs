﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PartyMaker.Models.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PartyMaker.Models
{
    public class PartyMakerDBContext: DbContext
    {
        public PartyMakerDBContext() : base("PartyMakerDB") { }

        public DbSet<Dish> Dishs { get; set; }
        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Kitchen> Kitchens { get; set; }
        public DbSet<RepresentativeAS> RepresentativeASs { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Venue> Venues { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}