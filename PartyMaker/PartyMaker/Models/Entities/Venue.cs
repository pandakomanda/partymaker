﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class Venue
    {
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public string VenueAdress { get; set; }
        public string VenueRating { get; set; }
        public string VenueType { get; set; }
        public byte[] VenuePhoto { get; set; }

        public virtual List<Drink> Drinks { get; set; }
        public virtual List<Kitchen> Kitchens { get; set; }
    }
}