﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class Drink
    {
        public int DrinkId { get; set; }
        public string DrinkName { get; set; }
        public decimal DrinkPrice { get; set; }
        public string DrinkCategory { get; set; }
        public string DrinkIngredients { get; set; }
        public byte[] DrinkPhoto { get; set; }

        public Venue Venue { get; set; }
    }
}