﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class User
    {
        public int UserId { get; set; }
        public string UserFIO { get; set; }
        public string UserEmail { get; set; }
        public byte[] UserPhoto { get; set; }

        public virtual List<Event> Events { get; set; }
    }
}