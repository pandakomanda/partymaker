﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class Kitchen
    {
        public int KitchenId { get; set; }
        public string KitchenName { get; set; }
        public string KitchenDescription { get; set; } 
        public Venue Venue { get; set; }
        public virtual List<Dish> Dishes { get; set; }
    }
}