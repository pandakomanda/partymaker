﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class Dish
    {
        public int DishId { get; set; }
        public string DishName { get; set; }
        public string DishDescription { get; set; }
        public string DishIngredients { get; set; }
        public byte[] DishPhoto { get; set; }
        public Kitchen Kitchen { get; set; } 
    }
}