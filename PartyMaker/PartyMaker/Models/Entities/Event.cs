﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class Event
    {
        public int EventId { get; set; }
        public string EventType { get; set; }
        public string EventName { get; set; }
        public decimal EventCost { get; set; }
        public string EventTime { get; set; }
        public string EventGuests { get; set; }

        public User User { get; set; }
        public virtual List<Service> Services { get; set; }
        public Venue Venue { get; set; }
    }
}