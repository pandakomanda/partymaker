﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class RepresentativeAS
    {
        public int RepresentativeASId { get; set; }
        public string RepresentativeASType { get; set; }
        public string RepresentativeASName { get; set; }
        public string RepresentativeASRating { get; set; }
        public byte[] RepresetnativeASPhoto { get; set; }

        public virtual List<Service> Services { get; set; }
    }
}