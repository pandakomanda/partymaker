﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PartyMaker.Models.Entities
{
    public class Service
    {
        public int ServiceId { get; set; }
        public string ServiceType { get; set; }
        public string ServiceName { get; set; }
        public decimal ServiceCost { get; set; }
        public byte[] ServicePhoto { get; set; }

        public Event Event { get; set; }

        public virtual List<RepresentativeAS> RepresentativeASs { get; set; }
    }
}